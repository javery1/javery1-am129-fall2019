! Student name: Jason Avery - Driver
! Student name: Alec Siegel - Navigator
! Group name: 344-4


program pi
    implicit none
    real :: pi_true, pi_apprx, threshold,diff 
    integer n
    pi_true = acos(-1.d0)
   
    n = 0  
    
    !threshold = 1.e-8
    !threshold = 1.e-4
    threshold = 1.e-12
    !threshold  = 1.e-16 
    
    pi_apprx = 0
    diff = abs(pi_apprx - pi_true)
    do while (diff > threshold) 
              diff = abs(pi_apprx - pi_true)
              pi_apprx = pi_apprx + (16. **(-n)) * ((4./(8.*n+1.))-(2./(8.*n+4.))-(1./(8.*n+5.))-(1./(8.*n+6.)))
              n = n+1
   
    enddo
    print*, "Threshold"
    print*, threshold
    print*, "Total number of summations"
    print*, n 
    print*, "Ending different"
    print*, diff
end program pi

