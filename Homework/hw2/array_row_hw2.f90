! Student name: Alec Siegel - Driver
! Student name: Jason Avery - Navigator 
! Group name: 344-4
! Can run makefile4 makefile5 or makefile6 or
! 
! gfortran -O3 -fdefault-real-8 -fdefault-double-8 array_row_hw2.f90
! gfortran -O0 -fdefault-real-8 -fdefault-double-8 array_row_hw2.f90
! gfortran -fdefault-real-8 -fdefault-double-8 -Wall -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace array_row_hw2.f90



program array_row
    integer, dimension (:,:), allocatable :: A, D, C1, C2
    integer n, i, j, count
    n = 8192
    !n = 8193 
    allocate(A(n,n))
    allocate(C1(n,n))
    allocate(C2(n,n))
    allocate(D(n,n))
    A = 0
    D = 0
    C1 = 0
    C2 = 0
    do 10 i = 1, n
        do 20 j = 1, n
            A(i,j) = i + j
            if (i==j) then
                D(i, i) = i + i
            endif
        20 continue
    10 continue

    do 30 i = 1, n
        do 70 j = 1, n
            C1(i,j) = A(i,j) * D(j,j)
        70 continue
    30 continue
    
    do 90 i = 1, n
        do 100 j = 1, n
           C2(j,i) = C2(j,i) + D(j,j)* A(i,j)    
            if (C2(i,j) - C1(i,j) .NE. 0) then
                count = count + 1
            endif 
        100 continue
    90 continue

print*, "The number of non zero terms after subtraction of the two matricies was:    "
print*, count
print*, "The expected number of non zero terms after subtraction of the two matri    cies was:"
print*, n*n - n

end program array_row
