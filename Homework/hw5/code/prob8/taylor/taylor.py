import os
import sys
import numpy as np
sys.path.append("/Users/jason/AM129/am129/homework/hw5/code/prob8/factorial")
from factorial import get_factorial



def exp_appx(x,n):
	if(n < 0):
                print("*** Invalid input: n must be non-negative integer")
                return  np.nan
	
	if(n == 0):
		return 1 
	else:
		apprx = x**n/(get_factorial(n)) + exp_appx(x,n-1)
		return apprx	


def sin_appx(x,n):
	if(n < 0):
                print("*** Invalid input: n must be non-negative integer")
                return  np.nan


	if(n == 0):
		return 0
	else:
		if(n%2 == 0):
			term = 0
		elif(n%4 == 1):
			term = 1
		else:
			term = -1
		 
		apprx = term * ((x**n)/get_factorial(n)) + sin_appx(x,n-1)
		return apprx	

def cos_appx(x,n):
	if(n < 0):
                print("*** Invalid input: n must be non-negative integer")
                return  np.nan
	
	if(n ==0):
		return 1

	else:
		if(n%2 == 1):
			term = 0
		elif(n%4 ==  0):
			term = 1
		else: 
			term = -1

		apprx = term * ((x**n)/get_factorial(n)) + cos_appx(x,n-1)
		return apprx
  

