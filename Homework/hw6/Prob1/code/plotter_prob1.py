#! /usr/bin/env python3
import collections as c
import matplotlib.pyplot as plt
import numpy as np

def word_count(file_path):
  with open(file_path, 'r') as fin:
    return c.Counter(fin.read().lower())



if __name__ == '__main__':  
  path = "./words.txt"
  hist = word_count(path)
  del hist['\n']
  listKeys = list(hist)
  hist = c.OrderedDict(sorted(hist.items()))
  tempList = list(hist.values())
  npVals = np.array(tempList)
  listKeys = sorted(listKeys)
  
  x = np.arange(len(listKeys))
  plt.xticks(x, listKeys)
  plt.xlabel('Character')
  plt.ylabel('Frequency')
  plt.bar(x, npVals)
  plt.grid(True)
  plt.plot(x, npVals, color = 'red', marker='o')
  plt.savefig('result_prob1.png', bbox_inches='tight')
