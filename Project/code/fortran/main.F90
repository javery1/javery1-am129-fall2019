program main

    use euler

    implicit none
    real :: t_0, t_f
    integer :: N
    character(len=35) :: ofile
    character(len=8) :: fmt, x1

    fmt = '(i2.2)'

    t_0 = 0.
    t_f = 10.
    N = 8
    
    do while (N <= 64)
        write (x1, fmt) N
        ofile = 'output_'//trim(x1)//'.dat'
        call eulers_method(t_0, t_f, N, ofile)

        N = N*2
    end do

end program main